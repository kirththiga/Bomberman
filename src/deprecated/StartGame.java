package deprecated;

import loginSystem.LoginMenuView;

/**
 * @author Philip Hoddinott <philip.hoddinott@mail.mcgill.ca>
 * @version 1.5 (current version number of program)
 * @since 2014-11-27 (the version of the package this class was first added to)
 */

public class StartGame {
    /**
     * This starts the game (1)
     * <p>
     * This method starts the LoginMenuViwer. It functions as a way to start the
     * game.
     * <p>
     * 
     * @return This does not return anything
     */
    public static void main(String[] args) {
	LoginMenuView.main(null);

    }
}
